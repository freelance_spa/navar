import React, { Component } from 'react';
import classNames from 'classnames';


class Filter extends Component {

	constructor(props) {
		super(props);
		this.state = {
			key: 'testrecommended'
		};
	}


	render() {
		return (
			<div className="filter">

				<div className="tab_filter">
					<ul className="nav nav-tabs">
						<li className={classNames({ active: this.state.key === 'testfollow' })}><a onClick={() => this.setState({ key: 'testfollow' })} data-toggle="tab">Follow</a></li>
						<li className={classNames({ active: this.state.key === 'testrecommended' })}><a onClick={() => this.setState({ key: 'testrecommended' })} data-toggle="tab">Recommended</a> </li>
					</ul>
				</div>

				<div className="tab_content">
					<div className="tab-content ">

						<div className={classNames('tab-pane', { active: this.state.key === 'testfollow' })} id="follow">
							<ul className="follow_recommended">
								<li className="active"><a href="#test">All</a></li>
								<li><a href="#test">Products</a></li>
								<li><a href="#test">Pinterest</a></li>
								<li><a href="#test">Instagram</a></li>
								<li><a href="#test">RSS</a></li>
								<li><a href="#test">Wordpress</a></li>
							</ul>
						</div>

						<div className={classNames('tab-pane', { active: this.state.key === 'testrecommended' })} id="recommended">
							<ul className="follow_recommended">
								<li className="active"><a href="#test">Products</a></li>
								<li><a href="#test">Pinterest</a></li>
								<li><a href="#test">Instagram</a></li>
								<li><a href="#test">RSS</a></li>
								<li><a href="#test">Wordpress</a></li>
							</ul>
						</div>

					</div>
				</div>

			</div>
		);
	}
}

export default Filter;