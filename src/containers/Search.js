import React, { Component } from 'react';

class Search extends Component {
	render() {
		return (
			<div className="search">
				<div className="input-group">
					<input type="text" className="form-control" placeholder="Search" />
					<span className="input-group-btn"> <button className="btn btn-secondary" type="button"><i className="fa fa-search" aria-hidden="true"></i></button> </span>
				</div>
			</div>
		);
	}
}

export default Search;