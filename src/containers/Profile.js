import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Profile extends Component {
	render() {
		return (
			<div className="profile_drop">
				<div className="profile_image hidden-xs" style={{ backgroundImage: 'url(https://s-media-cache-ak0.pinimg.com/564x/27/ee/32/27ee328df5f56540071cee83fa3da407.jpg)' }}></div>
				<div className="tab_content">
					<ul className="follow_recommended">
						<li className="active"><a href="#test">My Profile</a></li>
						<li><Link to="#">My shop</Link></li>
						<li><Link to="#">Connections</Link></li>
						<li><Link to="#">Prefferances</Link></li>
						<li><Link to="#">My sites</Link></li>
						<li><Link to="#">Log-out</Link></li>
					</ul>
				</div>
			</div>
		);
	}
}

export default Profile;