import React, { Component } from 'react';
import logo from './logo.svg';

import Filter from './containers/Filter';
import Profile from './containers/Profile';
import Search from './containers/Search';

import { Link } from 'react-router-dom';

const navs = {
  filter: Filter,
  profile: Profile,
  search: Search
};

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      key: ''
    };

    this._handleSubmenu = this._handleSubmenu.bind(this);
    this._handleClick = this._handleClick.bind(this);
    this._bodyClick = this._bodyClick.bind(this);
  }

  componentDidMount() {

  }

  _handleSubmenu(key) {

    window.addEventListener('click', this._bodyClick, false);

    this.setState({
      key
    });

  }

  _handleClick(e) {
    e.stopPropagation();
  }

  _bodyClick(e) {

    this.setState({
      key: ''
    });

    e.preventDefault();
  }

  render() {

    const Nav = navs[this.state.key];

    return (
      <nav className="navbar navbar-default navbar-static-top">
        <div className="container">

          <div className="navbar-header">
            <Link to="#" className="navbar-brand hidden-xs"> <img src={logo} alt="silkstaq logo" /> </Link>
          </div>

          <div id="navbar" className="">
            <ul className="nav navbar-nav navbar-right" onClick={(e) => e.stopPropagation()}>
              <li className="search_now"><a href="#test" onClick={() => this._handleSubmenu('search')}><i className="fa fa-search" aria-hidden="true"></i></a></li>
              <li><Link to="#"> Feed</Link></li>
              <li><Link to="#"> Shop</Link></li>
              <li className="filter_now"><a href="#test" onClick={() => this._handleSubmenu('filter')}><span className="hidden-xs">Filter</span> <i className="fa fa-sliders hidden-sm hidden-lg hidden-md" aria-hidden="true"></i></a></li>
              <li className="notification"><a href="#test"><i className="fa fa-bell" aria-hidden="true"></i> <span><i className="fa fa-circle" aria-hidden="true"></i></span></a></li>
              <li className="nav_profile_image"><a onClick={() => this._handleSubmenu('profile')} href="#test" style={{ backgroundImage: 'url(https://s-media-cache-ak0.pinimg.com/564x/27/ee/32/27ee328df5f56540071cee83fa3da407.jpg)' }}></a></li>
            </ul>
          </div>

          {Nav !== undefined ? (<div className="nav_drops" onClick={this._handleClick} onBlur={this._onBlur}><Nav /></div>) : null}

        </div>
      </nav>
    );
  }
}

export default App;
