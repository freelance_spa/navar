import React from 'react';
import ReactDOM from 'react-dom';

import './css/_silkstaq.css';
import './css/_silkstaq_custom.css';
import './css/_silkstaq_min_width_768.css';
import './css/_silkstaq_max_width_767.css';
import './css/_silkstaq_max_width_600.css';

import { BrowserRouter as Router } from 'react-router-dom';

import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
	<Router>
		<App />
	</Router>
	, document.getElementById('root'));
registerServiceWorker();
